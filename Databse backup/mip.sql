-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 28, 2021 at 01:55 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mip`
--

-- --------------------------------------------------------

--
-- Table structure for table `dataset`
--

CREATE TABLE `dataset` (
  `Dataset_id` int(11) NOT NULL,
  `Id` int(11) NOT NULL,
  `Location` varchar(200) NOT NULL,
  `Dated` datetime NOT NULL,
  `Dataset_date` date NOT NULL,
  `Dataset_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dataset`
--

INSERT INTO `dataset` (`Dataset_id`, `Id`, `Location`, `Dated`, `Dataset_date`, `Dataset_name`) VALUES
(5, 3, 'C:/Users/chetr/MCA _FINAL/imageprocessing/Uploaded_Dataset/Mango Field32021-06-08', '2021-06-08 21:01:13', '2021-06-08', 'Mango Field'),
(6, 3, 'C:/Users/chetr/MCA _FINAL/imageprocessing/Uploaded_Dataset/Apple Field32021-06-08', '2021-06-08 21:10:57', '2021-06-08', 'Apple Field'),
(7, 3, 'C:/Users/chetr/MCA _FINAL/imageprocessing/Uploaded_Dataset/Corn Field32021-06-16', '2021-06-17 10:21:42', '2021-06-16', 'Corn Field'),
(11, 5, 'C:/Users/chetr/MCA _FINAL/imageprocessing/Uploaded_Dataset/Rice Field52021-06-17', '2021-06-17 10:33:00', '2021-06-17', 'Rice Field'),
(12, 3, 'C:/Users/chetr/MCA _FINAL/imageprocessing/Uploaded_Dataset/NDRE DATASET32021-06-30', '2021-06-30 19:46:07', '2021-06-30', 'NDRE DATASET'),
(13, 8, 'C:/Users/chetr/MCA _FINAL/imageprocessing/Uploaded_Dataset/Guava Field82021-07-22', '2021-07-23 10:20:26', '2021-07-22', 'Guava Field');

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `Id` int(11) NOT NULL,
  `User_id` int(11) NOT NULL,
  `Email_Id` varchar(100) NOT NULL,
  `Contact` bigint(15) NOT NULL,
  `Feedback` varchar(200) NOT NULL,
  `Dated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`Id`, `User_id`, `Email_Id`, `Contact`, `Feedback`, `Dated`) VALUES
(1, 3, 'afa123@gmail.com', 9847584758, 'Request for a demo', '2021-07-27 10:09:34'),
(2, 3, 'afa123@gmail.com', 9876456374, 'No', '2021-07-27 19:01:31');

-- --------------------------------------------------------

--
-- Table structure for table `output`
--

CREATE TABLE `output` (
  `id` int(11) NOT NULL,
  `Dataset_id` int(11) NOT NULL,
  `Dataset_name` varchar(200) NOT NULL,
  `User_id` int(11) NOT NULL,
  `Ndvi_Ravel` varchar(800) NOT NULL,
  `Ndre_Ravel` varchar(800) NOT NULL,
  `EVI_Ravel` varchar(200) NOT NULL,
  `SAVI_Ravel` varchar(200) NOT NULL,
  `Max_ndvi` varchar(100) NOT NULL,
  `Min_ndvi` varchar(100) NOT NULL,
  `Max_ndre` varchar(100) NOT NULL,
  `Min_ndre` varchar(100) NOT NULL,
  `Max_Evi` varchar(100) NOT NULL,
  `Min_Evi` varchar(100) NOT NULL,
  `Max_Savi` varchar(100) NOT NULL,
  `Min_Savi` varchar(100) NOT NULL,
  `Output_Location` varchar(250) NOT NULL,
  `Dated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `output`
--

INSERT INTO `output` (`id`, `Dataset_id`, `Dataset_name`, `User_id`, `Ndvi_Ravel`, `Ndre_Ravel`, `EVI_Ravel`, `SAVI_Ravel`, `Max_ndvi`, `Min_ndvi`, `Max_ndre`, `Min_ndre`, `Max_Evi`, `Min_Evi`, `Max_Savi`, `Min_Savi`, `Output_Location`, `Dated`) VALUES
(3, 6, 'Apple Field', 3, 'static/Output/Apple Field32021-06-08/ndvi_ravel.txt', 'static/Output/Apple Field32021-06-08/ndre_ravel.txt', 'static/Output/Apple Field32021-06-08/evi_ravel.txt', 'static/Output/Apple Field32021-06-08/savi_ravel.txt', '0.9248997122049332', '-0.10968761891126633', '0.8296226784586906', '0.04057051055133343', '1.0492673218250275', '0.45', '0.8309869691729546', '0.0847005732357502', 'static/Output/Apple Field32021-06-08/', '2021-07-15 19:50:28'),
(4, 12, 'NDRE DATASET', 3, 'static/Output/NDRE DATASET32021-06-30/ndvi_ravel.txt', '0', 'static/Output/NDRE DATASET32021-06-30/evi_ravel.txt', 'static/Output/NDRE DATASET32021-06-30/savi_ravel.txt', '0', '0.07979086861014366', '0', '0', '0.9040458640456197', '0.45', '0.7762282684445373', '0.061535323597490796', 'static/Output/NDRE DATASET32021-06-30/', '2021-07-17 19:48:06'),
(5, 5, 'Mango Field', 3, 'static/Output/Mango Field32021-06-08/ndvi_ravel.txt', '0', '0', '0', '0.9248997122049332', '0.12300236150622368', '0', '0', '0', '0', '0', '0', 'static/Output/Mango Field32021-06-08/', '2021-07-12 22:55:35'),
(6, 13, 'Guava Field', 8, 'static/Output/Guava Field82021-07-22/ndvi_ravel.txt', '0', '0', '0', '0.9248997122049332', '0.12300236150622368', '0', '0', '0', '0', '0', '0', 'static/Output/Guava Field82021-07-22/', '2021-07-23 10:37:30');

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE `user_details` (
  `Id` int(11) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Dates` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`Id`, `Email`, `Password`, `Dates`) VALUES
(3, 'afa123@gmail.com', '123', '2021-06-02 12:39:38'),
(4, 'pramod2@gmail.com', '123', '2021-06-02 12:39:38'),
(5, 'pramodchetri9000@gmail.com', '123', '2021-06-03 20:31:06'),
(8, 'chetripramod28@gmail.com', '12345', '2021-07-23 10:18:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dataset`
--
ALTER TABLE `dataset`
  ADD PRIMARY KEY (`Dataset_id`),
  ADD KEY `Id` (`Id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `User_id` (`User_id`);

--
-- Indexes for table `output`
--
ALTER TABLE `output`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Dataset_id` (`Dataset_id`),
  ADD KEY `User_id` (`User_id`);

--
-- Indexes for table `user_details`
--
ALTER TABLE `user_details`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dataset`
--
ALTER TABLE `dataset`
  MODIFY `Dataset_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `output`
--
ALTER TABLE `output`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user_details`
--
ALTER TABLE `user_details`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `dataset`
--
ALTER TABLE `dataset`
  ADD CONSTRAINT `dataset_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `user_details` (`Id`);

--
-- Constraints for table `feedback`
--
ALTER TABLE `feedback`
  ADD CONSTRAINT `feedback_ibfk_1` FOREIGN KEY (`User_id`) REFERENCES `user_details` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `output`
--
ALTER TABLE `output`
  ADD CONSTRAINT `output_ibfk_1` FOREIGN KEY (`Dataset_id`) REFERENCES `dataset` (`Dataset_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `output_ibfk_2` FOREIGN KEY (`User_id`) REFERENCES `user_details` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
