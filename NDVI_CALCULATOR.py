#!/usr/bin/env python
# coding: utf-8
import json
import numpy as np

class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

def ndvi_calculator(input_loc):
    import io, os, sys, types
    from IPython import get_ipython
    import glob
    import micasense.capture as capture
    import cv2
    import json
    import numpy as np
    import matplotlib.pyplot as plt
    import micasense.imageutils as imageutils
    import micasense.plotutils as plotutils
    from osgeo import gdal, gdal_array
    from micasense import plotutils


    panelNames = None 
    
    image_prefix='IMG_0001'
    final_image_prefix=image_prefix+'_*.tif';

    panel_prefix='IMG_0000'
    final_panel_prefix=panel_prefix+'_*.tif';

    imagePath=input_loc;

    output_loc = os.path.basename(os.path.normpath(imagePath))
    print(output_loc)
    parent_dir = 'static/Output/'

    path = os.path.join(parent_dir, output_loc)
    final_path = path+'/';

    imageNames = glob.glob(os.path.join(imagePath,final_image_prefix)) #includes all images with prefix IMG_0001_ALL.TIF
    panelNames = glob.glob(os.path.join(imagePath,final_panel_prefix)) #includes all images with prefix IMG_0000_ALL.TIF

    if panelNames is not None:
        panelCap = capture.Capture.from_filelist(panelNames)
    else:
        panelCap = None

    capture = capture.Capture.from_filelist(imageNames)
    
    
    if panelCap is not None:
        if panelCap.panel_albedo() is not None:
            panel_reflectance_by_band = panelCap.panel_albedo() # Auto detect the panel and if not proceed to else with manual declaration
        else:
            panel_reflectance_by_band = [0.67, 0.69, 0.68, 0.61, 0.67] #RedEdge band_index order
        panel_irradiance = panelCap.panel_irradiance(panel_reflectance_by_band)    
        img_type = "reflectance"
        capture.plot_undistorted_reflectance(panel_irradiance)
    else:
        if capture.dls_present():
            img_type='reflectance'
            capture.plot_undistorted_reflectance(capture.dls_irradiance())
        else:
            img_type = "radiance"
            capture.plot_undistorted_radiance()
            
    

    ## Performing The Alignment settings

    match_index = 1 # Index of the band 
    max_alignment_iterations = 10
    warp_mode = cv2.MOTION_HOMOGRAPHY # MOTION_HOMOGRAPHY or MOTION_AFFINE. For Altum images only use HOMOGRAPHY
    pyramid_levels = 0 # for images with RigRelatives, setting this to 0 or 1 may improve alignment

    print("Alinging images. Depending on settings this can take from a few seconds to many minutes")
    warp_matrices, alignment_pairs = imageutils.align_capture(capture,
                                                              ref_index = match_index,
                                                              max_iterations = max_alignment_iterations,
                                                              warp_mode = warp_mode,
                                                              pyramid_levels = pyramid_levels)

    print("Aligining complete")
    
    cropped_dimensions, edges = imageutils.find_crop_bounds(capture, warp_matrices, warp_mode=warp_mode)
    im_aligned = imageutils.aligned_capture(capture, warp_matrices, warp_mode, cropped_dimensions, match_index, img_type=img_type)
    
    figsize=(16,13)   # using this size for export-sized display

    rgb_band_indices = [capture.band_names_lower().index('red'),
                        capture.band_names_lower().index('green'),
                        capture.band_names_lower().index('blue')]
    cir_band_indices = [capture.band_names_lower().index('nir'),
                        capture.band_names_lower().index('red'),
                        capture.band_names_lower().index('green')]

    # Create a normalized stack for viewing
    im_display = np.zeros((im_aligned.shape[0],im_aligned.shape[1],im_aligned.shape[2]), dtype=np.float32 )

    im_min = np.percentile(im_aligned[:,:,rgb_band_indices].flatten(), 0.5)  # modify these percentiles to adjust contrast
    im_max = np.percentile(im_aligned[:,:,rgb_band_indices].flatten(), 99.5)  # for many images, 0.5 and 99.5 are good values

    # for rgb true color, we use the same min and max scaling across the 3 bands to 
    # maintain the "white balance" of the calibrated image
    
    for i in rgb_band_indices:
        im_display[:,:,i] =  imageutils.normalize(im_aligned[:,:,i], im_min, im_max)

    rgb = im_display[:,:,rgb_band_indices]

    # for cir false color imagery, we normalize the NIR,R,G bands within themselves, which provides
    # the classical CIR rendering where plants are red and soil takes on a blue tint
    for i in cir_band_indices:
        im_display[:,:,i] =  imageutils.normalize(im_aligned[:,:,i])

    cir = im_display[:,:,cir_band_indices]

    print("Panel Calculation Executed")
    
    # Create an enhanced version of the RGB render using an unsharp mask
    gaussian_rgb = cv2.GaussianBlur(rgb, (9,9), 10.0)
    gaussian_rgb[gaussian_rgb<0] = 0
    gaussian_rgb[gaussian_rgb>1] = 1
    unsharp_rgb = cv2.addWeighted(rgb, 1.5, gaussian_rgb, -0.5, 0)
    unsharp_rgb[unsharp_rgb<0] = 0
    unsharp_rgb[unsharp_rgb>1] = 1

    # Apply a gamma correction to make the render appear closer to what our eyes would see
    gamma = 1.4
    gamma_corr_rgb = unsharp_rgb**(1.0/gamma)
    
    import imageio
    imtype = 'png' # or 'jpg'
    if os.path.isdir(path) == False:
        os.mkdir(path)
        imageio.imwrite(final_path+'/rgb.'+imtype, (255*gamma_corr_rgb).astype('uint8'))
        imageio.imwrite(final_path+'/cir.'+imtype, (255*cir).astype('uint8'))
    else:
        imageio.imwrite(final_path+'/rgb.'+imtype, (255*gamma_corr_rgb).astype('uint8'))
        imageio.imwrite(final_path+'/cir.'+imtype, (255*cir).astype('uint8'))
    print("Image write done")
    
    from osgeo import gdal, gdal_array
    rows, cols, bands = im_display.shape
    driver = gdal.GetDriverByName('GTiff')
    filename = "bgrne" #blue,green,red,nir,redEdge

    if im_aligned.shape[2] == 6:
        filename = filename + "t" #thermal

    outRaster = driver.Create(final_path+'/'+filename+".tiff", cols, rows, im_aligned.shape[2], gdal.GDT_UInt16)

    normalize = (img_type == 'radiance') # normalize radiance images to fit with in UInt16

    # Output a 'stack' in the same band order as RedEdge/Alutm
    # Blue,Green,Red,NIR,RedEdge[,Thermal]
    # reflectance stacks are output with 32768=100% reflectance to provide some overhead for specular reflections
    # radiance stacks are output with 65535=100% radiance to provide some overhead for specular reflections

    # NOTE: NIR and RedEdge are not in wavelength order!

    multispec_min = np.min(im_aligned[:,:,1:5])
    multispec_max = np.max(im_aligned[:,:,1:5])

    for i in range(0,5):
        outband = outRaster.GetRasterBand(i+1)
        if normalize:
            outdata = imageutils.normalize(im_aligned[:,:,i],multispec_min,multispec_max)
        else:
            outdata = im_aligned[:,:,i]
            outdata[outdata<0] = 0
            outdata[outdata>2] = 2

        outdata = outdata*32767
        outdata[outdata<0] = 0
        outdata[outdata>65535] = 65535
        outband.WriteArray(outdata)
        outband.FlushCache()

    if im_aligned.shape[2] == 6:
        outband = outRaster.GetRasterBand(6)
        outdata = im_aligned[:,:,5] * 100 # scale to centi-C to fit into uint16
        outdata[outdata<0] = 0
        outdata[outdata>65535] = 65535
        outband.WriteArray(outdata)
        outband.FlushCache()
    outRaster = None
    
    
    nir_band = capture.band_names_lower().index('nir')
    red_band = capture.band_names_lower().index('red')

    np.seterr(divide='ignore', invalid='ignore') # ignore divide by zero errors in the index calculation


    # Compute Normalized Difference Vegetation Index (NDVI) from the NIR(3) and RED (2) bands
    ndvi = (im_aligned[:,:,nir_band] - im_aligned[:,:,red_band]) / (im_aligned[:,:,nir_band] + im_aligned[:,:,red_band])

    # remove shadowed areas (mask pixels with NIR reflectance < 20%))
    if img_type == 'reflectance':
        ndvi = np.ma.masked_where(im_aligned[:,:,nir_band] < 0.20, ndvi) 
    elif img_type == 'radiance':
        lower_pct_radiance = np.percentile(im_aligned[:,:,3],  10.0)
        ndvi = np.ma.masked_where(im_aligned[:,:,nir_band] < lower_pct_radiance, ndvi) 


    # Compute a histogram
    ndvi_hist_min = np.min(ndvi)
    ndvi_hist_max = np.max(ndvi)

    #min_display_ndvi = 0.45 # further mask soil by removing low-ndvi values
    min_display_ndvi = np.percentile(ndvi.flatten(),  5.0)  # modify with these percentilse to adjust contrast
    max_display_ndvi = np.percentile(ndvi.flatten(), 99.5)  # for many images, 0.5 and 99.5 are good values
    masked_ndvi = np.ma.masked_where(ndvi < min_display_ndvi, ndvi)

    #reduce the figure size to account for colorbar
    figsize=np.asarray(figsize) - np.array([3,2])

    #plot NDVI over an RGB basemap, with a colorbar showing the NDVI scale

    fig, axis = plotutils.plot_overlay_withcolorbar(gamma_corr_rgb, 
                                              masked_ndvi, 
                                              figsize = figsize, 
                                              title = 'NDVI filtered to plants and soil over RGB base layer',
                                              vmin = min_display_ndvi,
                                              vmax = max_display_ndvi)

    print("Max-ndvi value=",max_display_ndvi)
    print("Min-ndvi value=",min_display_ndvi)
    print("Execution completed")
    
    max_display_ndvi_int=float(max_display_ndvi)
    min_display_ndvi_int=float(min_display_ndvi)
    ndvi_hist_max_int=float(ndvi_hist_max)
    ndvi_hist_min_int=float(ndvi_hist_min)
    ndvi_hist_ravel=ndvi.ravel()
    
    #print(type(ndvi_hist_ravel))

    
    if os.path.isdir(path) == False:
        os.mkdir(path)
        print(final_path)
        fig.savefig(final_path+'ndvi_over_rgb.png')
    else:
        fig.savefig(final_path+'ndvi_over_rgb.png')

    json_value = {
        'max_display_ndvi': max_display_ndvi_int,
        'min_display_ndvi': min_display_ndvi_int,
        'ndvi_hist_max': ndvi_hist_max_int,
        'ndvi_hist_min': ndvi_hist_min_int,
        'ndvi_ravel':ndvi_hist_ravel,
        'output_path':final_path
    }
    return json.dumps(json_value, cls=NumpyEncoder)
