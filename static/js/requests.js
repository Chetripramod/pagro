$('#get_profile').click(function(){
	 $.ajax({
            type: 'POST',
            url: '/get_profile',
            //data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {

			var obj = JSON.parse(data);
			console.log(obj)
			//console.log(obj.user_email);
			//console.log(obj.date);
            $('#user_email').attr ('value', obj.user_email); 
            $('#user_date_of_signup').attr ('value', obj.date);
            $('#user_email_contact').attr('value',obj.user_email);
            },
            error: function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            alert('Error - ' + errorMessage);
        }
    });
});
$('#get_profile_datasets').click(function(){
	 $.ajax({
            type: 'POST',
            url: '/get_profile_datasets',
            //data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {

			console.log(typeof data);
            const obj = JSON.parse(data);
			console.log(typeof obj);
            console.log(obj);

            /*for(var i = 0, size = obj.length; i < size ; i++){*/
            const item = obj;
            console.log("Items:",item);
            console.log(typeof item);
            const tb = $("#modal_data");
            let tr = [];
            let i = 0;
            item.forEach(item => {
                i++;
                tr.push('<tr><td><input type="checkbox" id="select_checkbox"/></td><td class="row-value" type="hidden">' + i + '</td><td class="row-value-id" style="display:none;">' + item['0'] + '</td><td class="row-value">' + item[5] + '</td><td class="row-value">' + item[2] + '</td><td class="row-value">' + item[4] + '</td><td class="row-value">' + item[3] + '</td></tr>');
            })
            console.log(tr)
            tb.append(tr);
            },
            error: function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            alert('Error - ' + errorMessage);
        }
    });
});

$('#modal_close').click(function(){
    const tb = $("td");
    tb.remove();
});

$('#proceed').click(function(event){
     var values = [];
    
    $('table #select_checkbox:checked').each(function () {
        var rowValue = $(this).closest('tr').find('td.row-value-id').text();
        values.push(rowValue);
    });
    
    console.log(values);
    $.ajax({
            type: 'POST',
            url: '/proceed',
            data: values,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {
                console.log("Data",data);
                //document.write(data);
                /*window.location.assign(data);*/
                //window.location.href= "Processing.html?"+data;
                /*$('#message').html('test')*/
                window.location.href = '/processing_dataset?data='+ values;
                console.log("success");
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                alert('Error - ' + errorMessage);
        }
    });
});