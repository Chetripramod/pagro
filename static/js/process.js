$('#connect_us').click(function(){
     $.ajax({
            type: 'POST',
            url: '/get_profile',
            //data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {

            var obj = JSON.parse(data);
            console.log(obj)
            //console.log(obj.user_email);
            //console.log(obj.date);
            $('#user_email_contact').attr('value',obj.user_email);
            },
            error: function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            alert('Error - ' + errorMessage);
        }
    });
});


$('#processed_datasets').click(function(){
    console.log("called");
	 $.ajax({
            type: 'POST',
            url: '/get_processed_datasets_details',
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {
    			console.log(typeof data);
                const obj = JSON.parse(data);
    			console.log(typeof obj);
                console.log(obj);
                
                const item = obj;
                console.log("Items:",item);
                console.log(typeof item);
                const tb = $("#modal_data");
                let tr = [];
                let i = 0;
                item.forEach(item => {
                    i++;
                    if(item[4] != "0"){
                        Ndvi = "<i class = 'fa fa-check' style='display:flex;justify-content:center;color:green;font-size:1.4rem;'></i>";
                    }
                    if(item[5] != "0"){
                        Ndre = "<i class = 'fa fa-check' style='display:flex;justify-content:center;color:green;font-size:1.4rem;'></i>";
                    }
                    if(item[6] != "0"){
                        Evi = "<i class = 'fa fa-check' style='display:flex;justify-content:center;color:green;font-size:1.4rem;'></i>";
                    }
                    if(item[7] != "0"){
                        Savi = "<i class = 'fa fa-check' style='display:flex;justify-content:center;color:green;font-size:1.4rem;'></i>";
                    }
                    if(item[4] === "0"){
                        Ndvi = "<i class = 'fa fa-close' style='display:flex;justify-content:center;color:red;font-size:1.4rem;'></i>";
                    }
                    if(item[5] === "0"){
                        Ndre = "<i class = 'fa fa-close' style='display:flex;justify-content:center;color:red;font-size:1.4rem;'></i>";
                    }
                    if(item[6] === "0"){
                        Evi = "<i class = 'fa fa-close' style='display:flex;justify-content:center;color:red;font-size:1.4rem;'></i>";
                    }
                    if(item[7] === "0"){
                        Savi = "<i class = 'fa fa-close' style='display:flex;justify-content:center;color:red;font-size:1.4rem;'></i>";
                    }
                    tr.push('<tr><td><input type="checkbox" id="select_checkbox"/></td><td class="row-value" type="hidden">' + i + '</td><td class="row-value-id" style="display:none;">' + item[1] + '</td><td class="row-value">' + item[2] + '</td><td class="row-value">' + Ndvi + '</td><td class="row-value">' + Ndre + '</td><td class="row-value">' + Evi + '</td><td class="row-value">' + Savi + '</td></tr>');
                })
                console.log(tr)
                tb.append(tr);
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                alert('Error - ' + errorMessage);
        }
    });
});

$('#modal_close').click(function(){
    const tb = $("td");
    tb.remove();
});

$('#proceed_analysis').click(function(event){
    var values = [];
    
    $('table #select_checkbox:checked').each(function () {
        var rowValue = $(this).closest('tr').find('td.row-value-id').text();
        values.push(rowValue);
        console.log(values);
    });
    console.log(values[0]);
    console.log(values[1]);
    if(values[0] === "" || values[1] === ""){
        alert("Please select two dataset for analysis");
    }
    var data = JSON.stringify({
                      "data_1": values[0],
                      "data_2": values[1],
    });
    $.ajax({
            type: 'POST',
            url: '/proceed_analysis',
            data: data,
            contentType: "application/json; charset=UTF-8",
            cache: false,
            processData: false,
            async: true,
            success: function (data) {
                var obj = JSON.parse(data);
                data1 = obj.data1;
                data2 = obj.data2;
                window.location.href = '/analysis?data1='+ data1 +'&data2=' +data2+'&';
                console.log("success");
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                alert('Error - ' + errorMessage);
        }
    });
});

$(document).ready(function() {
    
    // If the comparison slider is present on the page lets initialise it, this is good you will include this in the main js to prevent the code from running when not needed
    if ($(".comparison-slider")[0]) {
        let compSlider = $(".comparison-slider");
    
        //let's loop through the sliders and initialise each of them
        compSlider.each(function() {
            let compSliderWidth = $(this).width() + "px";
            $(this).find(".resize img").css({ width: compSliderWidth });
            drags($(this).find(".divider"), $(this).find(".resize"), $(this));
        });

        //if the user resizes the windows lets update our variables and resize our images
        $(window).on("resize", function() {
            let compSliderWidth = compSlider.width() + "px";
            compSlider.find(".resize img").css({ width: compSliderWidth });
        });
    }
});

// This is where all the magic happens
// This is a modified version of the pen from Ege Görgülü - https://codepen.io/bamf/pen/jEpxOX - and you should check it out too.
function drags(dragElement, resizeElement, container) {
    
    // This creates a variable that detects if the user is using touch input insted of the mouse.
    let touched = false;
    window.addEventListener('touchstart', function() {
        touched = true;
    });
    window.addEventListener('touchend', function() {
        touched = false;
    });
    
    // clicp the image and move the slider on interaction with the mouse or the touch input
    dragElement.on("mousedown touchstart", function(e) {
            
            //add classes to the emelents - good for css animations if you need it to
            dragElement.addClass("draggable");
            resizeElement.addClass("resizable");
            //create vars
            let startX = e.pageX ? e.pageX : e.originalEvent.touches[0].pageX;
            let dragWidth = dragElement.outerWidth();
            let posX = dragElement.offset().left + dragWidth - startX;
            let containerOffset = container.offset().left;
            let containerWidth = container.outerWidth();
            let minLeft = containerOffset + 10;
            let maxLeft = containerOffset + containerWidth - dragWidth - 10;
            
            //add event listner on the divider emelent
            dragElement.parents().on("mousemove touchmove", function(e) {
                
                // if the user is not using touch input let do preventDefault to prevent the user from slecting the images as he moves the silder arround.
                if ( touched === false ) {
                    e.preventDefault();
                }
                
                let moveX = e.pageX ? e.pageX : e.originalEvent.touches[0].pageX;
                let leftValue = moveX + posX - dragWidth;

                // stop the divider from going over the limits of the container
                if (leftValue < minLeft) {
                    leftValue = minLeft;
                } else if (leftValue > maxLeft) {
                    leftValue = maxLeft;
                }

                let widthValue = (leftValue + dragWidth / 2 - containerOffset) * 100 / containerWidth + "%";

                $(".draggable").css("left", widthValue).on("mouseup touchend touchcancel", function() {
                    $(this).removeClass("draggable");
                    resizeElement.removeClass("resizable");
                });
                
                $(".resizable").css("width", widthValue);
                
            }).on("mouseup touchend touchcancel", function() {
                dragElement.removeClass("draggable");
                resizeElement.removeClass("resizable");
                
            });
        
        }).on("mouseup touchend touchcancel", function(e) {
            // stop clicping the image and move the slider
            dragElement.removeClass("draggable");
            resizeElement.removeClass("resizable");
        
        });
    
}
