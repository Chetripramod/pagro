function getUrlVars() {
var url_string = window.location;
var url = new URL(url_string);
var name = url.searchParams.get("data1");
var tvid = url.searchParams.get("data2");
document.getElementById('dataset1').value=name;
document.getElementById('dataset2').value=tvid;
if(name === "" || tvid === "")
{
	alert("Please select two dataset for analysis.");
}
}
function show_ndvi_info(){
$('.overlay_new').show();
$('#overlay_text').text('Analysing dataset.! Please be patient')
var dataset1=$('#dataset1').val();
var dataset2=$('#dataset2').val();
var data = JSON.stringify({
"dataset1": dataset1,
"dataset2": dataset2
});
$.ajax({
type: 'POST',
url: '/show_ndvi_info',
data: data,
contentType : "application/json; charset=UTF-8",
cache: false,
processData: false,
async: true,
success: function (data) {
var obj = JSON.parse(data);
console.log(typeof(obj));
console.log(data);
if(obj.ndvi_ravel_dataset1 === '0'){
alert(obj.ndvi_name1+" is not processed with ndvi.!");
}
else if(obj.ndvi_ravel_dataset2 === '0'){
alert(obj.ndvi_name1+" is not processed with ndvi.!");
}

else{
console.log("inside");
var src1=obj.output_location+"/ndvi_over_rgb.png";
var src2=obj.output_location2+"/ndvi_over_rgb.png";
$('#img1').attr("src", src1);
$('#img2').attr("src", src2);
}
console.log(obj.max_ndvi_dataset1);
$('#output_location').val(obj.output_location);
$('#output_location2').val(obj.output_location2);
$('#analysis_max').text('Max Ndvi');
$('#analysis_min').text('Min Ndvi');
$('#analysis_max1').text('Max Ndvi');
$('#analysis_min1').text('Min Ndvi');
$('#img1_datasetname').text(obj.ndvi_name1);
$('#img2_datasetname').text(obj.ndvi_name2);
$('#dataset_name1').text(obj.ndvi_name1);
$('#dataset_name2').text(obj.ndvi_name2);
$('#b').val(obj.ndvi_ravel_dataset1);
$('#f').val(obj.ndvi_ravel_dataset2);
$('#c').val(obj.max_ndvi_dataset1);
$('#g').val(obj.max_ndvi_dataset2);
$('#d').val(obj.min_ndvi_dataset1);
$('#h').val(obj.min_ndvi_dataset2);
$('#name1').text(obj.ndvi_name1);
$('#name2').text(obj.ndvi_name2);
$('#before').text(obj.ndvi_name1);
$('#after').text(obj.ndvi_name2);
$('#img1').css("padding","12px");
if(obj.ndvi_ravel_dataset1 != '0' && obj.ndvi_ravel_dataset2 != '0'){
console.log(obj.ndvi_ravel_dataset1);
console.log(obj.ndvi_ravel_dataset2);
var x1 = obj.ndvi_ravel_dataset1;
var x2 = obj.ndvi_ravel_dataset2;
var name1 = obj.ndvi_name1;
var name2 = obj.ndvi_name2;
var trace1 = {
x: x1,
name : name1,
type: "histogram",
opacity: 0.5,
marker: {
color: 'yellow',
},
};
var trace2 = {
x: x2,
name : name2,
type: "histogram",
opacity: 0.6,
marker: {
color: 'red',
},
};
var data = [trace1, trace2];
var layout = {barmode: "overlay"};
Plotly.newPlot('plot', data, layout);
$('.overlay_new').hide();
}
},
error: function(xhr, status, error){
$('.overlay').hide();
var errorMessage = xhr.status + ': ' + xhr.statusText;
alert('Error - ' + errorMessage);
}
})
}
function show_ndre_info(){
$('.overlay_new').show();
$('#overlay_text').text('Analysing dataset.! Please be patient')
var dataset1=$('#dataset1').val();
var dataset2=$('#dataset2').val();
var data = JSON.stringify({
"dataset1": dataset1,
"dataset2": dataset2
});
$.ajax({
type: 'POST',
url: '/show_ndre_info',
data: data,
contentType : "application/json; charset=UTF-8",
cache: false,
processData: false,
async: true,
success: function (data) {
console.log(data);
var obj = JSON.parse(data);
if(obj.ndre_ravel_dataset1 === '0'){
console.log(obj.ndre_name1)
alert(obj.ndre_name1+" is not processed with ndre.!");
}
else if(obj.ndre_ravel_dataset2 === '0'){
console.log(obj.ndre_name2)
alert(obj.ndre_name2+" is not processed with ndre.!");
}
else{
console.log("inside");
var src1=obj.output_location+"/ndre_over_rgb.png";
var src2=obj.output_location2+"/ndre_over_rgb.png";
$('#img1').attr("src", src1);
$('#img2').attr("src", src2);
}
$('#output_location').val(obj.output_location);
$('#output_location2').val(obj.output_location2);
$('#analysis_max').text('Max Ndre');
$('#analysis_min').text('Min Ndre');
$('#analysis_max1').text('Max Ndre');
$('#analysis_min1').text('Min Ndre');
$('#dataset_name1').text(obj.ndre_name1);
$('#dataset_name2').text(obj.ndre_name2);
$('#b').val(obj.ndre_ravel_dataset1);
$('#f').val(obj.ndre_ravel_dataset2);
$('#c').val(obj.max_ndre_dataset1);
$('#g').val(obj.max_ndre_dataset2);
$('#d').val(obj.min_ndre_dataset1);
$('#h').val(obj.min_ndre_dataset2);
$('#before').text(obj.ndre_name1);
$('#after').text(obj.ndre_name2);
$('#img1').css("padding","12px");
if(obj.ndre_ravel_dataset1 != '0' && obj.ndre_ravel_dataset2 != '0'){
var x1 = obj.ndre_ravel_dataset1;
var x2 = obj.ndre_ravel_dataset2;
var name1 = obj.ndre_name1;
var name2 = obj.ndre_name2;
var trace1 = {
x: x1,
name : name1,
type: "histogram",
opacity: 0.5,
marker: {
color: 'yellow',
},
};
var trace2 = {
x: x2,
name: name2,
type: "histogram",
opacity: 0.6,
marker: {
color: 'red',
},
};
var data = [trace1, trace2];
var layout = {barmode: "overlay"};
Plotly.newPlot('plot', data, layout);
$('.overlay_new').hide();
}
},
error: function(xhr, status, error){
$('.overlay').hide();
var errorMessage = xhr.status + ': ' + xhr.statusText;
alert('Error - ' + errorMessage);
}
})
}
function show_evi_info(){
console.log('clicked');
$('.overlay_new').show();
$('#overlay_text').text('Analysing dataset.! Please be patient')
var dataset1=$('#dataset1').val();
var dataset2=$('#dataset2').val();
var data = JSON.stringify({
"dataset1": dataset1,
"dataset2": dataset2
});
$.ajax({
type: 'POST',
url: '/show_evi_info',
data: data,
contentType : "application/json; charset=UTF-8",
cache: false,
processData: false,
async: true,
success: function (data) {
console.log(data);
var obj = JSON.parse(data);
if(obj.evi_ravel_dataset1 === '0'){
console.log(obj.ndre_name1)
alert(obj.evi_name1+" is not processed with evi.!");
}
else if(obj.evi_ravel_dataset2 === '0'){
console.log(obj.evi_name2)
alert(obj.evi_name2+" is not processed with evi.!");
}
else{
console.log("inside");
var src1=obj.output_location+"/evi_over_rgb.png";
var src2=obj.output_location2+"/evi_over_rgb.png";
$('#img1').attr("src", src1);
$('#img2').attr("src", src2);
}
$('#output_location').val(obj.output_location);
$('#output_location2').val(obj.output_location2);
$('#analysis_max').text('Max evi');
$('#analysis_min').text('Min evi');
$('#analysis_max1').text('Max evi');
$('#analysis_min1').text('Min evi');
$('#dataset_name1').text(obj.evi_name1);
$('#dataset_name2').text(obj.evi_name2);
$('#b').val(obj.evi_ravel_dataset1);
$('#f').val(obj.evi_ravel_dataset2);
$('#c').val(obj.max_evi_dataset1);
$('#g').val(obj.max_evi_dataset2);
$('#d').val(obj.min_evi_dataset1);
$('#h').val(obj.min_evi_dataset2);
$('#before').text(obj.evi_name1);
$('#after').text(obj.evi_name2);
$('#img1').css("padding","12px");
if(obj.evi_ravel_dataset1 != '0' && obj.evi_ravel_dataset2 != '0'){
var x1 = obj.evi_ravel_dataset1;
var x2 = obj.evi_ravel_dataset2;
console.log(x1);
console.log(x2);
var name1 = obj.evi_name1;
var name2 = obj.evi_name2;
var trace1 = {
x: x1,
name : name1,
type: "histogram",
opacity: 0.5,
marker: {
color: 'yellow',
},
};
var trace2 = {
x: x2,
name: name2,
type: "histogram",
opacity: 0.6,
marker: {
color: 'red',
},
};
var data = [trace1, trace2];
var layout = {barmode: "overlay"};
Plotly.newPlot('plot', data, layout);
$('.overlay_new').hide();
}
},
error: function(xhr, status, error){
$('.overlay').hide();
var errorMessage = xhr.status + ': ' + xhr.statusText;
alert('Error - ' + errorMessage);
}
})
}
function show_savi_info(){
console.log('clicked');
$('.overlay_new').show();
$('#overlay_text').text('Analysing dataset.! Please be patient')
var dataset1=$('#dataset1').val();
var dataset2=$('#dataset2').val();
var data = JSON.stringify({
"dataset1": dataset1,
"dataset2": dataset2
});
$.ajax({
type: 'POST',
url: '/show_savi_info',
data: data,
contentType : "application/json; charset=UTF-8",
cache: false,
processData: false,
async: true,
success: function (data) {
console.log(data);
var obj = JSON.parse(data);
if(obj.ndre_ravel_dataset1 === '0'){
console.log(obj.ndre_name1)
alert(obj.savi_name1+" is not processed with savi.!");
}
else if(obj.savi_ravel_dataset2 === '0'){
console.log(obj.savi_name2)
alert(obj.savi_name2+" is not processed with savi.!");
}
else{
console.log("inside");
var src1=obj.output_location+"/savi_over_rgb.png";
var src2=obj.output_location2+"/savi_over_rgb.png";
$('#img1').attr("src", src1);
$('#img2').attr("src", src2);
}
$('#output_location').val(obj.output_location);
$('#output_location2').val(obj.output_location2);
$('#analysis_max').text('Max savi');
$('#analysis_min').text('Min savi');
$('#analysis_max1').text('Max savi');
$('#analysis_min1').text('Min savi');
$('#dataset_name1').text(obj.savi_name1);
$('#dataset_name2').text(obj.savi_name2);
$('#b').val(obj.savi_ravel_dataset1);
$('#f').val(obj.savi_ravel_dataset2);
$('#c').val(obj.max_savi_dataset1);
$('#g').val(obj.max_savi_dataset2);
$('#d').val(obj.min_savi_dataset1);
$('#h').val(obj.min_savi_dataset2);
$('#before').text(obj.savi_name1);
$('#after').text(obj.savi_name2);
$('#img1').css("padding","12px");
if(obj.savi_ravel_dataset1 != '0' && obj.savi_ravel_dataset2 != '0'){
var x1 = obj.savi_ravel_dataset1;
var x2 = obj.savi_ravel_dataset2;
var name1 = obj.savi_name1;
var name2 = obj.savi_name2;
var trace1 = {
x: x1,
name: name1,
type: "histogram",
opacity: 0.5,
marker: {
color: 'yellow',
},
};
var trace2 = {
x: x2,
name : name2,
type: "histogram",
opacity: 0.6,
marker: {
color: 'red',
},
};
var data = [trace1, trace2];
var layout = {barmode: "overlay"};
Plotly.newPlot('plot', data, layout);
$('.overlay').hide();
}
},
error: function(xhr, status, error){
    $('.overlay').hide();
	var errorMessage = xhr.status + ': ' + xhr.statusText;
	alert('Error - ' + errorMessage);
	}
})
}
/*REFLECTANCE*/
function show_rgbreflectance()
{
	var src_d1 = document.getElementById('output_location').value;
	var src_d2 = document.getElementById('output_location2').value;
	if(src_d1 != "" && src_d2 != ""){
	var src1=src_d1+"/rgb.png";
	var src2=src_d2+"/rgb.png";
	$('#img1').attr("src", src1);
	$('#img2').attr("src", src2);
	}
}
function show_cirreflectance()
{
	var src_d1 = document.getElementById('output_location').value;
	var src_d2 = document.getElementById('output_location2').value;
	if(src_d1 != "" && src_d2 != ""){
	var src1=src_d1+"/cir.png";
	var src2=src_d2+"/cir.png";
	$('#img1').attr("src", src1);
	$('#img2').attr("src", src2);
	}
}
function show_chart(){
	document.getElementById('plot').style.display='block';
}
function close_chart(){
	document.getElementById('plot').style.display='none';
}
function exportDashboard() {
	var doc = new jsPDF('p','mm','a4');
	var src1 = document.getElementById('img1');
	var src2 = document.getElementById('img2');
	var d1 = document.getElementById('dataset_name1').innerHTML;
	var d2 = document.getElementById('dataset_name2').innerHTML;
	var d1_max = document.getElementById('c').value;
	var d2_max = document.getElementById('g').value;
	var d1_min = document.getElementById('d').value;
	var d2_min = document.getElementById('h').value;
    doc.setFontSize(24);
    doc.setTextColor(153, 0, 153);
    doc.text(80, 15, "PyAgro Analysis");
    doc.setFontSize(14);
    doc.text(30,35, d1);
    doc.addImage(src2, 'JPEG', 5, 40, 100, 120);
    doc.text(20,160,"Max Value= "+ d1_max);
    doc.text(20,170,"Min Value= "+ d1_min);
    doc.text(130,35, d2);
    doc.addImage(src1, 'JPEG', 105, 40, 100, 120);
    doc.text(120,160,"Max Value= "+ d2_max);
    doc.text(120,170,"Min Value= "+ d2_min);
	doc.save('dashboard.pdf');
}
$(document).ready(function() {
    
    // If the comparison slider is present on the page lets initialise it, this is good you will include this in the main js to prevent the code from running when not needed
    if ($(".comparison-slider")[0]) {
        let compSlider = $(".comparison-slider");
    
        //let's loop through the sliders and initialise each of them
        compSlider.each(function() {
            let compSliderWidth = $(this).width() + "px";
            $(this).find(".resize img").css({ width: compSliderWidth });
            drags($(this).find(".divider"), $(this).find(".resize"), $(this));
        });

        //if the user resizes the windows lets update our variables and resize our images
        $(window).on("resize", function() {
            let compSliderWidth = compSlider.width() + "px";
            compSlider.find(".resize img").css({ width: compSliderWidth });
        });
    }
});

// This is where all the magic happens
// This is a modified version of the pen from Ege Görgülü - https://codepen.io/bamf/pen/jEpxOX - and you should check it out too.
function drags(dragElement, resizeElement, container) {
    
    // This creates a variable that detects if the user is using touch input insted of the mouse.
    let touched = false;
    window.addEventListener('touchstart', function() {
        touched = true;
    });
    window.addEventListener('touchend', function() {
        touched = false;
    });
    
    // clicp the image and move the slider on interaction with the mouse or the touch input
    dragElement.on("mousedown touchstart", function(e) {
            
            //add classes to the emelents - good for css animations if you need it to
            dragElement.addClass("draggable");
            resizeElement.addClass("resizable");
            //create vars
            let startX = e.pageX ? e.pageX : e.originalEvent.touches[0].pageX;
            let dragWidth = dragElement.outerWidth();
            let posX = dragElement.offset().left + dragWidth - startX;
            let containerOffset = container.offset().left;
            let containerWidth = container.outerWidth();
            let minLeft = containerOffset + 10;
            let maxLeft = containerOffset + containerWidth - dragWidth - 10;
            
            //add event listner on the divider emelent
            dragElement.parents().on("mousemove touchmove", function(e) {
                
                // if the user is not using touch input let do preventDefault to prevent the user from slecting the images as he moves the silder arround.
                if ( touched === false ) {
                    e.preventDefault();
                }
                
                let moveX = e.pageX ? e.pageX : e.originalEvent.touches[0].pageX;
                let leftValue = moveX + posX - dragWidth;

                // stop the divider from going over the limits of the container
                if (leftValue < minLeft) {
                    leftValue = minLeft;
                } else if (leftValue > maxLeft) {
                    leftValue = maxLeft;
                }

                let widthValue = (leftValue + dragWidth / 2 - containerOffset) * 100 / containerWidth + "%";

                $(".draggable").css("left", widthValue).on("mouseup touchend touchcancel", function() {
                    $(this).removeClass("draggable");
                    resizeElement.removeClass("resizable");
                });
                
                $(".resizable").css("width", widthValue);
                
            }).on("mouseup touchend touchcancel", function() {
                dragElement.removeClass("draggable");
                resizeElement.removeClass("resizable");
                
            });
        
        }).on("mouseup touchend touchcancel", function(e) {
            // stop clicping the image and move the slider
            dragElement.removeClass("draggable");
            resizeElement.removeClass("resizable");
        
        });
    
}