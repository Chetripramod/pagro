$(document).ready(function () {
    // Predict
    $('#btn-predict-ndvi').click(function () {
        //var form_data = new FormData($('#upload-file')[0]);
        $('#report_display').css("display","block");
        // Show loading animation
        /*$(this).hide();*/
        $('.overlay_new').show();
        $('#overlay_text').text('Processing dataset.! Please be patient')

        // Make prediction by calling api /predict

        var input_loc = $('#result3').val();
        console.log(input_loc)
        $.ajax({
            type: 'POST',
            url: '/predict',
            data: input_loc,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {
                //console.log(data)
                //data_parsed=JSON.parse(data)
                // Get and display the result
                var obj = JSON.parse(data);
                console.log(obj)
                $('.overlay_new').hide();
                /*$('#result').fadeIn(600);*/
                //$('#result').text(' Total Data Obtained:  ' + obj);
                $('#val0').text('NDVI PROCESSING');
                $('#max_text').text('MAX NDVI VALUE');
                $('#min_text').text('MIN NDVI VALUE');
                $('#val1').text(obj.max_display_ndvi);
                $('#val2').text(obj.min_display_ndvi);
                $('#val3').text(obj.ndvi_hist_max);
                $('#val4').text(obj.ndvi_hist_min);
                $('#val5').text(obj.output_path);
                $('#val6').text(obj.ndvi_ravel);
                $('#before').text('NDVI Image');
                $('#after').text('RGB Image');
                /*$('#val5').text(' NDVI RAVEL :  ' + obj.ndvi_ravel);*/
                var src=obj.output_path+"/ndvi_over_rgb.png";
                var rgb_src=obj.output_path+"/rgb.png";
                $('#ndvi_image').attr("src", src);
                $('#rgb_image').attr("src",rgb_src);
                $("#rgb_image").css("padding", "60px");
                $("#rgb_image").css("margin-left", "-20px");
                var x = obj.ndvi_ravel;
                console.log(typeof(x));
                var data = [
                  {
                    x: x,
                    type: 'histogram',
                    histnorm: 'probability',
                    marker: {
                        color: 'rgb(255,255,100)',
                        width: '200px'
                     },
                  }
                ];
                Plotly.newPlot('myDiv', data);
                //console.log('Success!');
            },
            error: function(xhr, status, error){
                 $('.overlay_new').hide();
                 var errorMessage = xhr.status + ': ' + xhr.statusText;
                 alert('Error - ' + errorMessage);
             }
        });
    });
    $('#btn-predict-ndre').click(function () {
        //var form_data = new FormData($('#upload-file')[0]);
        $('#report_display').css("display","block");
        // Show loading animation
        /*$(this).hide();*/
        $('.overlay_new').show();
        $('#overlay_text').text('Processing dataset.! Please be patient')

        // Make prediction by calling api /predict

        var input_loc = $('#result3').val();
        console.log(input_loc)
        $.ajax({
            type: 'POST',
            url: '/predict_ndre',
            data: input_loc,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {
                var obj = JSON.parse(data);
                console.log(obj)
                $('.overlay_new').hide();
                $('#val0').text(' NDRE PROCESSING');
                $('#max_text').text('MAX NDRE VALUE');
                $('#min_text').text('MIN NDRE VALUE');
                $('#val1').text(obj.max_display_ndre);
                $('#val2').text(obj.min_display_ndre);
                $('#val3').text(obj.ndre_hist_max);
                $('#val4').text(obj.ndre_hist_min);
                $('#val5').text(obj.output_path);
                $('#val6').text(obj.ndre_ravel);
                $('#before').text('NDRE Image');
                $('#after').text('RGB Image');
                var src=obj.output_path+"/ndre_over_rgb.png";
                $('#ndvi_image').attr('src', src);
                var rgb_src = obj.output_path+"/rgb.png";
                $('#rgb_image').attr("src",rgb_src);
                $("#rgb_image").css("padding", "60px");
                $("#rgb_image").css("margin-left", "-20px");
                //console.log('Success!');
                var x = obj.ndre_ravel;
                var data = [
                  {
                    x: x,
                    type: 'histogram',
                    histnorm: 'probability',
                    marker: {
                        color: 'rgb(255,255,100)',
                        width: '200px'
                     },
                  }
                ];
                Plotly.newPlot('myDiv', data);
                //console.log('Success!');
            },
            error: function(xhr, status, error){
                 $('.overlay_new').hide();
                 var errorMessage = xhr.status + ': ' + xhr.statusText;
                 alert('Error - ' + errorMessage);
             }
        });
    });
    $('#btn-predict-evi').click(function () {
        //var form_data = new FormData($('#upload-file')[0]);
        $('#report_display').css("display","block");
        // Show loading animation
        /*$(this).hide();*/
        $('.overlay_new').show();
        $('#overlay_text').text('Processing dataset.! Please be patient')

        // Make prediction by calling api /predict

        var input_loc = $('#result3').val();
        console.log(input_loc)
        $.ajax({
            type: 'POST',
            url: '/predict_evi',
            data: input_loc,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {
                var obj = JSON.parse(data);
                console.log(obj)
                $('.overlay_new').hide();
                $('#val0').text(' EVI PROCESSING');
                $('#max_text').text('MAX EVI VALUE');
                $('#min_text').text('MIN EVI VALUE');
                $('#val1').text(obj.max_display_evi);
                $('#val2').text(obj.min_display_evi);
                $('#val3').text(obj.evi_hist_max);
                $('#val4').text(obj.evi_hist_min);
                $('#val5').text(obj.output_path);
                $('#val6').text(obj.evi_ravel);
                $('#before').text('EVI Image');
                $('#after').text('RGB Image');
                var src=obj.output_path+"/evi_over_rgb.png";
                $('#ndvi_image').attr('src', src);
                var rgb_src = obj.output_path+"/rgb.png";
                $('#rgb_image').attr("src",rgb_src);
                $("#rgb_image").css("padding", "60px");
                $("#rgb_image").css("margin-left", "-20px");
                //console.log('Success!');
                var x = obj.evi_ravel;
                var data = [
                  {
                    x: x,
                    type: 'histogram',
                    histnorm: 'probability',
                    marker: {
                        color: 'rgb(255,255,100)',
                        width: '200px'
                     },
                  }
                ];
                Plotly.newPlot('myDiv', data);
                //console.log('Success!');
            },
            error: function(xhr, status, error){
                 $('.overlay_new').hide();
                 var errorMessage = xhr.status + ': ' + xhr.statusText;
                 alert('Error - ' + errorMessage);
             }
        });
    });
    $('#btn-predict-savi').click(function () {
        //var form_data = new FormData($('#upload-file')[0]);
        $('#report_display').css("display","block");
        // Show loading animation
        /*$(this).hide();*/
        $('.overlay_new').show();
        $('#overlay_text').text('Processing dataset.! Please be patient')
        // Make prediction by calling api /predict

        var input_loc = $('#result3').val();
        console.log(input_loc)
        $.ajax({
            type: 'POST',
            url: '/predict_savi',
            data: input_loc,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {
                var obj = JSON.parse(data);
                console.log(obj)
                $('.overlay_new').hide();
                $('#val0').text(' SAVI PROCESSING');
                $('#max_text').text('MAX SAVI VALUE');
                $('#min_text').text('MIN SAVI VALUE');
                $('#val1').text(obj.max_display_savi);
                $('#val2').text(obj.min_display_savi);
                $('#val3').text(obj.savi_hist_max);
                $('#val4').text(obj.savi_hist_min);
                $('#val5').text(obj.output_path);
                $('#val6').text(obj.savi_ravel);
                $('#before').text('SAVI Image');
                $('#after').text('RGB Image');
                var src=obj.output_path+"/savi_over_rgb.png";
                $('#ndvi_image').attr('src', src);
                var rgb_src = obj.output_path+"/rgb.png";
                $('#rgb_image').attr("src",rgb_src);
                $("#rgb_image").css("padding", "60px");
                $("#rgb_image").css("margin-left", "-20px");
                //console.log('Success!');
                var x = obj.savi_ravel;
                var data = [
                  {
                    x: x,
                    type: 'histogram',
                    histnorm: 'probability',
                    marker: {
                        color: 'rgb(255,255,100)',
                        width: '200px'
                     },
                  }
                ];
                Plotly.newPlot('myDiv', data);
                //console.log('Success!');
            },
            error: function(xhr, status, error){
                 $('.overlay_new').hide();
                 var errorMessage = xhr.status + ': ' + xhr.statusText;
                 alert('Error - ' + errorMessage);
             }
        });
    });

     $('#preprocess_type').click(function () {
        $('#report_display').css("display","none");
        $('.overlay_new').show();

        $('#overlay_text').text('Pre-processing dataset.! Please be patient');
        var input_loc = $('#result3').val();
        console.log(input_loc)
        $.ajax({
            type: 'POST',
            url: '/pre_process',
            data: input_loc,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {
                var obj = JSON.parse(data);
                console.log(obj)
                $('.overlay_new').hide();
                /*$('#val0').text(' NDRE PROCESSING');
                $('#max_text').text('MAX NDRE VALUE');
                $('#min_text').text('MIN NDRE VALUE');
                $('#val1').text(obj.max_display_ndre);
                $('#val2').text(obj.min_display_ndre);
                $('#val3').text(obj.ndre_hist_max);
                $('#val4').text(obj.ndre_hist_min);*/
                /*$('#val6').text(obj.ndre_ravel);*/
                $('#val5').text(obj.output_path);
                $('#before').text('Enhanced RGB Image');
                //$('#after').text('RGB Image');
                var src=obj.output_path+"enhanced_rgb.png";
                $('#ndvi_image').attr('src', src);

                var src2 = obj.output_path+"normal_rgb.png";
                $('#rgb_image').attr("src",src2);
                $("#rgb_image").css("padding", "0px");
                $('#after').text('Normal RGB Image');
               /* var rgb_src = obj.output_path+"/rgb.png";
                $('#rgb_image').attr("src",rgb_src);
                $("#rgb_image").css("padding", "60px");*/
                //console.log('Success!');
                /*var x = obj.ndre_ravel;
                var data = [
                  {
                    x: x,
                    type: 'histogram',
                    histnorm: 'probability',
                    marker: {
                        color: 'rgb(255,255,100)',
                        width: '200px'
                     },
                  }
                ];
                Plotly.newPlot('myDiv', data);*/
                //console.log('Success!');
            },
            error: function(xhr, status, error){
                 $('.overlay_new').hide();
                 var errorMessage = xhr.status + ': ' + xhr.statusText;
                 alert('Error - ' + errorMessage);
             }
        });
    });
});


