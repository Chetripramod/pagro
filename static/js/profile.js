$('#show_profile_datasets').click(function(){
	 $.ajax({
            type: 'POST',
            url: '/get_profile_datasets',
            //data: form_data,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {
            $("#dataset_info").css("display","block");
			console.log(typeof data);
            const obj = JSON.parse(data);
			console.log(typeof obj);
            console.log(obj);

            /*for(var i = 0, size = obj.length; i < size ; i++){*/
            const item = obj;
            console.log("Items:",item);
            console.log(typeof item);
            const tb = $("#modal_data");
            let tr = [];
            let i = 0;
            item.forEach(item => {
                i++;
                tr.push('<tr><td><input type="checkbox" id="select_checkbox"/></td><td class="row-value" type="hidden">' + i + '</td><td class="row-value-id" style="display:none;">' + item['0'] + '</td><td class="row-value">' + item[5] + '</td><td class="row-value">' + item[2] + '</td><td class="row-value">' + item[4] + '</td><td class="row-value">' + item[3] + '</td></tr>');
            })
            console.log(tr)
            tb.append(tr);
            },
            error: function(xhr, status, error){
            var errorMessage = xhr.status + ': ' + xhr.statusText;
            alert('Error - ' + errorMessage);
        }
    });
});

$("#delete_dataset").click(function(){
    var values = [];
    
    $('table #select_checkbox:checked').each(function () {
        var rowValue = $(this).closest('tr').find('td.row-value-id').text();
        values.push(rowValue);
    });
    $.ajax({
            type: 'POST',
            url: '/delete_dataset',
            data: values,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {
                window.location.href = '/profile';
                console.log("success");
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                alert('Error - ' + errorMessage);
        }
    });
});

$("#hide_dataset").click(function(){
    const tb = $("td");
    tb.remove();
    $("#dataset_info").css("display","none");
})

$("#update_dataset").click(function(){
    $("#modal_dataset").hide();
    var values = [];
    
    $('table #select_checkbox:checked').each(function () {
        var rowValue = $(this).closest('tr').find('td.row-value-id').text();
        values.push(rowValue);
    });
    $.ajax({
            type: 'POST',
            url: '/get_update_dataset_info',
            data: values,
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {
                $("#modal_dataset").show();
                console.log(data);
                const obj = JSON.parse(data);

                $("#dataset_name").val(obj.name);
                $("#dataset_location").val(obj.location);
                $("#date").attr("date", obj.date);
                console.log("success");
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                alert('Error - ' + errorMessage);
        }
    });
})

function get_details(){
    $.ajax({
            type: 'POST',
            url: '/get_user_details',
            contentType: false,
            cache: false,
            processData: false,
            async: true,
            success: function (data) {
                console.log(data);
                const obj = JSON.parse(data);

                var str = obj.email;
                var name = str.substring(0,6);
                $("#name").text(name);
                $("#full_name").text(name);
                $("#email_user").text(obj.email);
                $("#doj").text(obj.doj);
                console.log("success");
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText;
                alert('Error - ' + errorMessage);
        }
    });
}