# -*- coding: utf-8 -*-
"""
Created on Thu Jun 11 22:34:20 2020

@author: Pramod Chetri
"""

from __future__ import division, print_function
# coding=utf-8
import sys
import json
import os
import glob
import shutil
import re
import cv2
import numpy as np
import micasense.plotutils as plotutils
import http 
#import tensorflow as tf
#import tensorflow as tf

#from tensorflow.compat.v1 import ConfigProto
#from tensorflow.compat.v1 import InteractiveSession

#config = ConfigProto()
#config.gpu_options.per_process_gpu_memory_fraction = 0.2
#config.gpu_options.allow_growth = True
#session = InteractiveSession(config=config)
# Keras
#from tensorflow.keras.applications.resnet50 import preprocess_input
#from tensorflow.keras.models import load_model
#from tensorflow.keras.preprocessing import image

# Flask utils
from flask import Flask, redirect, url_for, request, render_template, jsonify, session 
from werkzeug.utils import secure_filename
#from gevent.pywsgi import WSGIServer
import NDVI_CALCULATOR as ndvi
import ndre_calculator as ndre
import SAVI_CALCULATOR as savi
import EVI_CALCULATOR as evi
import Image_Enhancement as ie
import mysql.connector as con
import datetime
# import arcpy


app = Flask(__name__)

# arcpy.CreateRandomRaster_management("C:/Users/chetr/MCA _FINAL/imageprocessing", "randrast", 
#                                     "NORMAL 3.0", "0 0 500 500", 50)

# Define a flask app

# Model saved with Keras model.save()
#MODEL_PATH ='model_resnet152V2.h5'

# Load your trained model
#model = load_model(MODEL_PATH)

connect_obj = con.connect(
        host="localhost",
        user="root",
        password="",
        database="MIP"
)

app.secret_key = 'WXCC2233DD3244'

class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)

@app.route('/', methods=['GET'])
def index():
    # Main page
    if connect_obj.is_connected():
        return render_template('index.html')
    else:
        print("Couldn't connect to the database")
        return render_template('404.html')

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    #signup
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']
        dated = datetime.datetime.now()

        mycursor = connect_obj.cursor()
        sql = "SELECT * FROM user_details WHERE Email= %s AND Password= %s" 
        # Fetch one record and return result
        val = (email,password)
        no_of_rows = mycursor.execute(sql,val)
        myresult = mycursor.fetchone()
        if myresult == None:
            sql = "INSERT INTO user_details (email, password, dates) VALUES (%s, %s, %s)"
            val = (email, password, dated)
            mycursor.execute(sql, val)
            connect_obj.commit()
            print(mycursor.rowcount, "record inserted.")
            return render_template('index.html',signup_msg="successfully signed up.!Login Now")
        else:
            return render_template('index.html',signup_msg="already signed up")
    else:
        return render_template('index.html',signup_msg="Request Not Accepted")

@app.route('/login',methods=['GET','POST'])
def login():
    #login
    if request.method == 'POST':
        Login_email = request.form['Login_email']
        Login_password = request.form['Login_password']

        mycursor = connect_obj.cursor()
        sql = "SELECT * FROM user_details WHERE Email= %s AND Password= %s" 
        # Fetch one record and return result
        val = (Login_email,Login_password)
        no_of_rows = mycursor.execute(sql,val)
        myresult = mycursor.fetchone()
        if myresult == None:
            msg = "Invalid Email Id/Password"
            return render_template('index.html',msg=msg)
        else:
            # Create session data, we can access this data in other routes
            session['loggedin'] = True
            session['id'] = myresult[0]
            session['Email'] = myresult[1]
            msg = "Logged in successfully"
            print(session['id'])
            return render_template('homepage.html',msg=msg)
    else:
        return render_template('homepage.html')


@app.route('/logout',methods=['GET','POST'])
def logout():
    if request.method == 'POST' or request.method == 'GET':
        session.pop('loggedin', None)
        session.pop('id', None)
        session.pop('Email', None)
        # Redirect to login page
        return render_template('index.html')


@app.route('/home', methods=['GET','POST'])
def home():
    if request.method == 'POST' or request.method == "GET":
         return render_template('homepage.html')
def model_predict(input_loc):
    print("processing")
    print(input_loc)
    preds = ndvi.ndvi_calculator(input_loc)
    print("This model predict function is called");
    return preds
 
@app.route('/predict', methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':

        input_loc = request.get_data(cache=True, as_text=True, parse_form_data=False)
        print(input_loc)
        preds = model_predict(input_loc)
        #preds=ndvi.ndvi_calculator(input_loc)
        result=preds
        json_data=json.loads(result)
       
        print("Upload Function is called")
        #print(json_data)

        # for key,values in json_data.items():
        #     print(key,values)
        return json.dumps(json_data)
    else:
        print("Error")

def ndre_model_predict(ndre_input_loc):
    print("NDRE Processing")
    print(ndre_input_loc)
    ndre_preds = ndre.ndre_calculator(ndre_input_loc)
    print("This model ndre predict function is called");
    return ndre_preds


@app.route('/predict_ndre',methods=['GET','POST'])
def process_ndre():
    if request.method == 'POST':
        ndre_input_loc = request.get_data(cache=True, as_text=True, parse_form_data=False)
        print("NDRE LOC = ",ndre_input_loc)
        ndre_preds = ndre_model_predict(ndre_input_loc)
        ndre_result = ndre_preds
        ndre_json_data = json.loads(ndre_result)
        
        print("NDRE PREDICT FUNCTION CALLED")

        return json.dumps(ndre_json_data)
    else:
        print("error")

def evi_model_predict(evi_input_loc):
    print("EVI Processing")
    print(evi_input_loc)
    evi_preds = evi.evi_calculator(evi_input_loc)
    print("This model evi predict function is called");
    return evi_preds


@app.route('/predict_evi',methods=['GET','POST'])
def process_evi():
    if request.method == 'POST':
        evi_input_loc = request.get_data(cache=True, as_text=True, parse_form_data=False)
        print("EVI LOC = ",evi_input_loc)
        evi_preds = evi_model_predict(evi_input_loc)
        evi_result = evi_preds
        evi_json_data = json.loads(evi_result)
        
        print("EVI PREDICT FUNCTION CALLED")

        return json.dumps(evi_json_data)
    else:
        print("error")

def savi_model_predict(savi_input_loc):
    print("SAVI Processing")
    print(savi_input_loc)
    savi_preds = savi.savi_calculator(savi_input_loc)
    print("This model savi predict function is called");
    return savi_preds


@app.route('/predict_savi',methods=['GET','POST'])
def process_savi():
    if request.method == 'POST':
        savi_input_loc = request.get_data(cache=True, as_text=True, parse_form_data=False)
        print("SAVI LOC = ",savi_input_loc)
        savi_preds = savi_model_predict(savi_input_loc)
        savi_result = savi_preds
        savi_json_data = json.loads(savi_result)
        
        print("SAVI PREDICT FUNCTION CALLED")

        return json.dumps(savi_json_data)
    else:
        print("error")

def pre_process(dataset_loc):
    print("Pre Processing")
    print(dataset_loc)
    preds = ie.img_enhance(dataset_loc)
    return preds

@app.route('/pre_process', methods=['GET','POST'])
def pre_process_image():
    if request.method == 'POST':
        dataset_loc = request.get_data(cache=True, as_text=True, parse_form_data=False)
        print("Dataset  Location=",dataset_loc)
        pre_preds = pre_process(dataset_loc)
        result = pre_preds
        json_data = json.loads(result)

        print("Function called")
        return json.dumps(json_data)
    else:
        print("error")

@app.route('/upload', methods=['GET','POST'])
def upload_dataset():
    if request.method == 'POST':
        location = request.form['dataset_location']
        dataset_dated = request.form['date_dataset']
        dataset_name = request.form['dataset_name']
        user_id = session['id']; 

        user_folder_id= str(user_id)
        folder_date= str(dataset_dated)
        final_location='Uploaded_Dataset/'+dataset_name+user_folder_id+folder_date
        os.mkdir(final_location)

        dated = datetime.datetime.now()
       
        # UPLOAD THE IMAGES TO FINAL_LOCATION FOLDER
        for tif in glob.iglob(os.path.join(location, "*.tif")):
            shutil.copy(tif, final_location)

        mycursor = connect_obj.cursor()
        if location == "" or dataset_dated == "" or user_id == "":
            return render_template("homepage.html",msg="Invalid details")
        try:
            sql = "INSERT INTO dataset (id, Location, dated, dataset_date, dataset_name) VALUES (%s, %s, %s, %s, %s)"
            val = (user_id, final_location, dated, dataset_dated, dataset_name)
            mycursor.execute(sql, val)
            connect_obj.commit()
            return render_template("homepage.html")
        except:
            return render_template("homepage.html",msg="Error uploading the dataset")
    else:
        return render_template("homepage.html")

@app.route('/get_profile',methods=['GET','POST'])
def get_user_data():
    if request.method == 'POST':
        user_id=session['id']
        mycursor = connect_obj.cursor()
        sql = "SELECT * FROM user_details WHERE id = %s"
        user_id_given=(user_id,)
        mycursor.execute(sql,user_id_given)
        myresult = mycursor.fetchone()
        print(myresult)
        user_email=myresult[1]
        date=str(myresult[3])
        print(user_email)
        print(date)
        json_value = {
            'user_email':user_email,
            'date':date
        }
        return json.dumps(json_value)

class DateTimeEncoder(json.JSONEncoder):
    def default(self, z):
        if isinstance(z, datetime.date):
            return (str(z))
        else:
            return super().default(z)

@app.route('/get_profile_datasets',methods=['GET','POST'])
def get_dataset():
    if request.method=='POST':
        user_id=session['id']
        if user_id != "":
            mycursor = connect_obj.cursor()
            print(session['id'])
            sql = "SELECT * FROM `dataset` WHERE Id = %s"
            user_id_given=(user_id,)
            mycursor.execute(sql,user_id_given)
            data = mycursor.fetchall()
            print("Total rows are:  ", len(data))
            count=0;
            
            if data != []:
                    print(type(data))
                    return json.dumps(data,indent = 6,cls=DateTimeEncoder)
            else:
                location = ""
                date = ""
                uploaded_date = ""
                dataset_name = ""

                json_value = {
                    'dataset_name':dataset_name,
                    'dataset_date':date,
                    'location':location,
                    'uploaded_date':uploaded_date
                }
                return json.dumps(json_value)
        else:
            return render_template('index.html')

@app.route('/proceed', methods=['GET','POST'])
def process():
    if request.method=='POST':
        try:
            print("The request is made");
            data = request.get_data(cache=True, as_text=True, parse_form_data=False)
            dataset_id=int(data);
            print(dataset_id)
            print(type(dataset_id))
            print("rendering process now")
            return redirect(url_for('processing.html', data=dataset_id))
        except:
            print("Exception")
            return render_template("homepage.html",msg="Error processing the dataset")
    else:
        print("Error")
        return render_template("homepage.html",msg="Error processing the dataset")


@app.route('/get_specific_dataset_detail', methods=['GET','POST'])
def get_specific_dataset():
    if request.method=='POST':
        print('Request')
        try:
            dataset_id=request.get_data(cache=True, as_text=True, parse_form_data=False)
            print(dataset_id)
            final_id=int(dataset_id)
            mycursor = connect_obj.cursor()
            
            sql = "SELECT * FROM dataset WHERE Dataset_id = %s"
            dataset_id_given=(final_id,)
            mycursor.execute(sql,dataset_id_given)
            data = mycursor.fetchall()
            print("Total rows are:  ", len(data))
            print(data)
            if data != []:
                    print(type(data))
                    return json.dumps(data,indent = 6,cls=DateTimeEncoder)
        except:
            print("error")
            return render_template('processing.html')

@app.route('/processing_dataset')
def processing(data = None):
    return render_template('processing.html', data=data)

@app.route('/proceed_analysis', methods=['GET','POST'])
def process_analysis():
    if request.method=='POST':
        try:
            print("The request is made");
            #data = request.get_data(cache=True, as_text=True, parse_form_data=False)
            #list_data = list(data)
            data = request.get_json();
            data1 = data['data_1'];
            data2 = data['data_2'];
            print("rendering process now")
            total_data = {
                'data1' : data1,
                'data2' : data2
            }
            #return redirect(url_for('analysis.html', data=data))
            return json.dumps(total_data)
        except:
            print("Exception")
            return render_template("homepage.html",msg="Error processing the dataset")
    else:
        print("Error")
        return render_template("homepage.html",msg="Error processing the dataset")

@app.route('/analysis')
def analysis(data = None):
    return render_template('analysis.html', data=data)

@app.route('/get_processed_datasets_details',methods=['GET','POST'])
def get_processed_dataset():
    if request.method=='POST':
        try:
            user_id=session['id']
            if user_id != "":
                mycursor = connect_obj.cursor()
                print(user_id)
                sql = "SELECT * FROM `output` WHERE `User_id` = %s"
                user_id_given=(user_id,)
                mycursor.execute(sql,user_id_given)
                data = mycursor.fetchall()
                print("Total rows are:  ", len(data))
                count=0;
                
                if data != []:
                        print(data)
                        print(type(data))
                        return json.dumps(data,indent = 6,cls=DateTimeEncoder)
                else:
                    location = ""
                    date = ""
                    uploaded_date = ""
                    dataset_name = ""

                    json_value = {
                        'dataset_name':dataset_name,
                        'dataset_date':date,
                        'location':location,
                        'uploaded_date':uploaded_date
                    }
                    return json.dumps(json_value)
            else:
                return render_template('processing.html')
        except:
            print("Error")
@app.route('/save_ndvi', methods=['GET','POST'])
def save_ndvi():
    if request.method == 'POST':
        # HERE USE 
        try:
            data = request.get_json()
            user_id = session['id']
            dataset_id = data['id']
            dataset_name = data['dataset_name']
            print("Data",dataset_name)
            ndvi_ravel = data['ndvi_ravel']
            max_ndvi = data['max_ndvi']
            min_ndvi = data['min_ndvi']
            output_location = data['output_location']
            Dated = datetime.datetime.now()
            mycursor = connect_obj.cursor(buffered=True)

            # print(dataset_id)
            # print(max_ndvi)
            # print(min_ndvi)
            # print(output_location)

            sql = "SELECT * FROM output WHERE Dataset_id = %s" 
            val = (dataset_id,)
            no_of_rows = mycursor.execute(sql,val)
            myresult = mycursor.fetchone()
            if myresult == None:
                with open(output_location+'ndvi_ravel.txt', 'w') as f:
                    f.write(str(ndvi_ravel))
                ndvi_ravel_new = output_location+'ndvi_ravel.txt';
                sql1 = "INSERT INTO output (Dataset_id, Dataset_name, User_id, Ndvi_Ravel, Ndre_Ravel, EVI_Ravel, SAVI_Ravel, Max_ndvi, Min_ndvi, Max_ndre, Min_ndre, Max_Evi, Min_Evi, Max_Savi, Min_savi, Output_Location, Dated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                val = (dataset_id, dataset_name, user_id, ndvi_ravel_new, "0", "0", "0", max_ndvi, min_ndvi , "0", "0", "0", "0", "0", "0", output_location, Dated)
                mycursor.execute(sql1, val)
                connect_obj.commit()
                return render_template("homepage.html")
            else:
                dataset_name = data['dataset_name']
                print(dataset_name)
                ndre_ravel = myresult[5]
                evi_ravel = myresult[6]
                savi_ravel = myresult[7]
                max_ndre = myresult[10]
                min_ndre = myresult[11]
                max_evi = myresult[12]
                min_evi = myresult[13]
                max_savi = myresult[14]
                min_savi = myresult[15]

                output_location = myresult[16]
                # print(ndre_ravel)
                # print(max_ndre)
                # print(min_ndre)
                # print(output_location)
                with open(output_location+'ndvi_ravel.txt', 'w') as f:
                    f.write(str(ndvi_ravel))
                ndvi_ravel_new = output_location+'ndvi_ravel.txt';
                #sql2 = "UPDATE output set (Ndvi_Ravel, Ndre_Ravel, Max_ndvi, Min_ndvi, Max_ndre, Min_ndre, Output_Location, Dated) VALUES (%s, %s, %s, %s, %s , %s, %s, %s) WHERE Dataset_id = %s"
                sql2 = "UPDATE output set Dataset_name = %s, User_id = %s, Ndvi_Ravel = %s , Ndre_Ravel = %s, EVI_Ravel = %s, SAVI_Ravel = %s, Max_ndvi = %s, Min_ndvi = %s, Max_ndre = %s, Min_ndre = %s, Max_Evi = %s, Min_Evi = %s, Max_Savi = %s, Min_savi = %s, Output_Location = %s, Dated = %s WHERE Dataset_id = %s"
                val = (dataset_name, user_id, ndvi_ravel_new, ndre_ravel, evi_ravel, savi_ravel, max_ndvi, min_ndvi, max_ndre, min_ndre, max_evi, min_evi, max_savi, min_savi, output_location, Dated, dataset_id)
                mycursor.execute(sql2, val)
                connect_obj.commit()
                print("updated")
                return render_template("homepage.html")
        except Exception as error:
            print("Error=",error)
            return render_template("homepage.html")

@app.route('/save_ndre', methods=['GET','POST'])
def save_ndre():
    if request.method == 'POST':
        # HERE USE 
        try:
            data = request.get_json()
            user_id = session['id']
            dataset_name = data['dataset_name']
            dataset_id = data['id']
            ndre_ravel = data['ndre_ravel']
            max_ndre = data['max_ndre']
            min_ndre = data['min_ndre']
            output_location = data['output_location']
            Dated = datetime.datetime.now()
            mycursor = connect_obj.cursor(buffered=True)

            # print(dataset_id)
            # print(max_ndre)
            # print(min_ndre)
            # print(output_location)

            sql = "SELECT * FROM output WHERE Dataset_id = %s" 
            val = (dataset_id,)
            no_of_rows = mycursor.execute(sql,val)
            myresult = mycursor.fetchone()
            if myresult == None:
                with open(output_location+'ndre_ravel.txt', 'w') as f:
                    f.write(str(ndre_ravel))
                ndre_ravel_new = output_location+'ndre_ravel.txt';
                sql1 = "INSERT INTO output (Dataset_id, Dataset_name, User_id, Ndvi_Ravel, Ndre_Ravel, EVI_Ravel, SAVI_Ravel, Max_ndvi, Min_ndvi, Max_ndre, Min_ndre, Max_Evi, Min_Evi, Max_Savi, Min_Savi, Output_Location, Dated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                val = (dataset_id, dataset_name, user_id, "0", ndre_ravel_new, "0", "0", "0" , "0" , max_ndre, min_ndre, "0", "0", "0", "0", output_location, Dated)
                mycursor.execute(sql1, val)
                connect_obj.commit()
                return render_template("homepage.html")
            else:
                dataset_name = myresult[2]
                ndvi_ravel = myresult[4]
                evi_ravel = myresult[6]
                savi_ravel = myresult[7]
                max_ndvi = myresult[8]
                min_ndvi = myresult[9]
                max_evi = myresult[12]
                min_evi = myresult[13]
                max_savi = myresult[14]
                min_savi = myresult[15]
                output_location = myresult[16]
                print(ndvi_ravel)
                # print(max_ndvi)
                # print(min_ndvi)
                # print(output_location)
                with open(output_location+'ndre_ravel.txt', 'w') as f:
                    f.write(str(ndre_ravel))
                ndre_ravel_new = output_location+'ndre_ravel.txt';
                #sql2 = "UPDATE output set (Ndvi_Ravel, Ndre_Ravel, Max_ndvi, Min_ndvi, Max_ndre, Min_ndre, Output_Location, Dated) VALUES (%s, %s, %s, %s, %s , %s, %s, %s) WHERE Dataset_id = %s"
                sql2 = "UPDATE output set User_id = %s, Ndvi_Ravel = %s , Ndre_Ravel = %s, EVI_ravel = %s, SAVI_Ravel = %s, Max_ndvi = %s, Min_ndvi = %s, Max_ndre = %s, Min_ndre = %s, Max_Evi = %s, Min_Evi = %s, Max_Savi = %s, Min_Savi = %s, Output_Location = %s, Dated = %s WHERE Dataset_id = %s"
                val = (user_id, ndvi_ravel, ndre_ravel_new, evi_ravel, savi_ravel, max_ndvi, min_ndvi, max_ndre, min_ndre, max_evi, min_evi, max_savi, min_savi, output_location, Dated, dataset_id)
                mycursor.execute(sql2, val)
                connect_obj.commit()
                print("updated")
                return render_template("homepage.html")
        except Exception as error:
            print("Error=",error)
            return render_template("homepage.html")

@app.route('/save_evi', methods=['GET','POST'])
def save_evi():
    if request.method == 'POST':
        # HERE USE 
        try:
            data = request.get_json()
            user_id = session['id']
            dataset_id = data['id']
            dataset_name = data['dataset_name']
            evi_ravel = data['evi_ravel']
            max_evi = data['max_evi']
            min_evi = data['min_evi']
            output_location = data['output_location']
            Dated = datetime.datetime.now()
            mycursor = connect_obj.cursor(buffered=True)

            # print(dataset_id)
            # print(max_ndre)
            # print(min_ndre)
            # print(output_location)

            sql = "SELECT * FROM output WHERE Dataset_id = %s" 
            val = (dataset_id,)
            no_of_rows = mycursor.execute(sql,val)
            myresult = mycursor.fetchone()
            if myresult == None:
                with open(output_location+'evi_ravel.txt', 'w') as f:
                    f.write(str(evi_ravel))
                evi_ravel_new = output_location+'ndre_ravel.txt';
                sql1 = "INSERT INTO output (Dataset_id, Dataset_name, User_id, Ndvi_Ravel, Ndre_Ravel, EVI_Ravel, SAVI_Ravel, Max_ndvi, Min_ndvi, Max_ndre, Min_ndre, Max_Evi, Min_Evi, Max_Savi, Min_Savi, Output_Location, Dated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                val = (dataset_id, dataset_name, user_id, "0", "0", evi_ravel_new, "0", "0" , "0" , "0", "0", max_evi, min_evi, "0", "0", output_location, Dated)
                mycursor.execute(sql1, val)
                connect_obj.commit()
                return render_template("homepage.html")
            else:
                dataset_name = myresult[2]
                ndvi_ravel = myresult[4]
                ndre_ravel = myresult[5]
                savi_ravel = myresult[7]
                max_ndvi = myresult[8]
                min_ndvi = myresult[9]
                max_ndre = myresult[10]
                min_ndre = myresult[11]
                max_savi = myresult[14]
                min_savi = myresult[15]
                output_location = myresult[16]
                # print(evi_ravel)
                # print(max_ndvi)
                # print(min_ndvi)
                # print(output_location)
                with open(output_location+'evi_ravel.txt', 'w') as f:
                    f.write(str(evi_ravel))
                evi_ravel_new = output_location+'evi_ravel.txt';
                #sql2 = "UPDATE output set (Ndvi_Ravel, Ndre_Ravel, Max_ndvi, Min_ndvi, Max_ndre, Min_ndre, Output_Location, Dated) VALUES (%s, %s, %s, %s, %s , %s, %s, %s) WHERE Dataset_id = %s"
                sql2 = "UPDATE output set Dataset_name = %s, User_id = %s,  Ndvi_Ravel = %s , Ndre_Ravel = %s, EVI_ravel = %s, SAVI_Ravel = %s, Max_ndvi = %s, Min_ndvi = %s, Max_ndre = %s, Min_ndre = %s, Max_Evi = %s, Min_Evi = %s, Max_Savi = %s, Min_Savi = %s, Output_Location = %s, Dated = %s WHERE Dataset_id = %s"
                val = (dataset_name, user_id, ndvi_ravel, ndre_ravel, evi_ravel_new, savi_ravel, max_ndvi, min_ndvi, max_ndre, min_ndre, max_evi, min_evi, max_savi, min_savi, output_location, Dated, dataset_id)
                mycursor.execute(sql2, val)
                connect_obj.commit()
                print("updated")
                return render_template("homepage.html")
        except Exception as error:
            print("Error=",error)
            return render_template("homepage.html")

@app.route('/save_savi', methods=['GET','POST'])
def save_savi():
    if request.method == 'POST':
        # HERE USE 
        try:
            data = request.get_json()
            user_id = session['id']
            print(user_id)
            dataset_id = data['id']
            dataset_name = data['dataset_name']
            savi_ravel = data['savi_ravel']
            max_savi = data['max_savi']
            min_savi = data['min_savi']
            output_location = data['output_location']
            Dated = datetime.datetime.now()
            mycursor = connect_obj.cursor(buffered=True)

            # print(dataset_id)
            # print(max_ndre)
            # print(min_ndre)
            # print(output_location)
            
            sql = "SELECT * FROM output WHERE Dataset_id = %s" 
            val = (dataset_id,)
            no_of_rows = mycursor.execute(sql,val)
            myresult = mycursor.fetchone()
            if myresult == None:
                with open(output_location+'savi_ravel.txt', 'w') as f:
                    f.write(str(savi_ravel))
                savi_ravel_new = output_location+'ndre_ravel.txt';
                print("inside if")
                sql1 = "INSERT INTO output (Dataset_id, Dataset_name, User_id, Ndvi_Ravel, Ndre_Ravel, EVI_Ravel, SAVI_Ravel, Max_ndvi, Min_ndvi, Max_ndre, Min_ndre, Max_Evi, Min_Evi, Max_Savi, Min_Savi, Output_Location, Dated) VALUES (%s, %s, %s, %s, %s, %s, %s, %s , %s, %s, %s, %s, %s, %s, %s, %s, %s)"
                val = (dataset_id, dataset_name, user_id, "0", "0", "0", savi_ravel_new, "0" , "0" , "0", "0", "0", "0", max_savi, min_savi, output_location, Dated)
                mycursor.execute(sql1, val)
                connect_obj.commit()
                return render_template("homepage.html")
            else:
                dataset_name = myresult[2]
                ndvi_ravel = myresult[4]
                ndre_ravel = myresult[5]
                evi_ravel = myresult[6]
                max_ndvi = myresult[7]
                min_ndvi = myresult[9]
                max_ndre = myresult[10]
                min_ndre = myresult[11]
                max_evi = myresult[12]
                min_evi = myresult[13]
                output_location = myresult[16]
                # print(evi_ravel)
                # print(max_ndvi)
                # print(min_ndvi)
                # print(output_location)
                with open(output_location+'savi_ravel.txt', 'w') as f:
                    f.write(str(savi_ravel))
                savi_ravel_new = output_location+'savi_ravel.txt';
                #sql2 = "UPDATE output set (Ndvi_Ravel, Ndre_Ravel, Max_ndvi, Min_ndvi, Max_ndre, Min_ndre, Output_Location, Dated) VALUES (%s, %s, %s, %s, %s , %s, %s, %s) WHERE Dataset_id = %s"
                sql2 = "UPDATE output set Dataset_name = %s, User_id = %s, Ndvi_Ravel = %s , Ndre_Ravel = %s, EVI_ravel = %s, SAVI_Ravel = %s, Max_ndvi = %s, Min_ndvi = %s, Max_ndre = %s, Min_ndre = %s, Max_Evi = %s, Min_Evi = %s, Max_Savi = %s, Min_Savi = %s, Output_Location = %s, Dated = %s WHERE Dataset_id = %s"
                val = (dataset_name, user_id, ndvi_ravel, ndre_ravel, evi_ravel, savi_ravel_new, max_ndvi, min_ndvi, max_ndre, min_ndre, max_evi, min_evi, max_savi, min_savi, output_location, Dated, dataset_id)
                mycursor.execute(sql2, val)
                connect_obj.commit()
                print("updated")
                return render_template("homepage.html")
        except Exception as error:
            print("Error=",error)
            return render_template("homepage.html")

@app.route('/show_ndvi_info', methods=['GET','POST'])
def show_ndvi_info():
        if request.method == 'POST':
            try:
                data = request.get_json()
                dataset1 = data['dataset1']
                dataset2 = data['dataset2']
                mycursor = connect_obj.cursor(buffered=True)
                sql = "SELECT * FROM output WHERE Dataset_id = %s" 
                val = (dataset1,)
                no_of_rows = mycursor.execute(sql,val)
                myresult = mycursor.fetchone()
                if myresult != None:
                    ndvi_name1 = myresult[2]
                    ndvi_ravel_dataset1 = myresult[4]
                    max_ndvi_dataset1 = myresult[8]
                    min_ndvi_dataset1 = myresult[9]
                    output_location = myresult[16]
                    if ndvi_ravel_dataset1 != '0': 
                        # with open(ndvi_ravel_dataset1,'r') as file:
                        #     ndvi_ravel = file.read()
                        # ndvi_ravel_array = np.asarray(ndvi_ravel)
                    #     with open(ndvi_ravel_dataset1, 'r') as f:
                    #         data = f.read().split()
                    #         ndvi_ravel1 = []
                    #         for elem in data:
                    #             try:
                    #                 ndvi_ravel1.append(elem)
                    #             except ValueError:
                    #                 pass
                    #         print(type(ndvi_ravel1))
                    # else:
                    #     ndvi_ravel1 = '0'
                            data = open(ndvi_ravel_dataset1,"r")
                            content = data.read()
                            ndvi_data = content.split(",")
                            for i in range(0, len(ndvi_data),1):
                                if ndvi_data[i] != '':
                                    ndvi_data[i] = float(ndvi_data[i])

                            #print(ndvi_data)
                    else:
                        ndvi_data = '0'

                    sql2 = "SELECT * FROM output WHERE Dataset_id = %s" 
                    val2 = (dataset2,)
                    no_of_rows2 = mycursor.execute(sql,val2)
                    myresult2 = mycursor.fetchone()
                    if myresult2 != None:
                        ndvi_name2 = myresult2[2]
                        ndvi_ravel_dataset2 = myresult2[4]
                        max_ndvi_dataset2 = myresult2[8]
                        min_ndvi_dataset2 = myresult2[9]
                        output_location2 = myresult2[16]
                        print(output_location2)
                        if ndvi_ravel_dataset2 != '0':
                            # with open(ndvi_ravel_dataset2, 'r') as f:
                            #     data = f.read().split()
                            #     ndvi_ravel2 = []
                            #     for elem in data:
                            #         try:
                            #             ndvi_ravel2.append(elem)
                            #         except ValueError:
                            #             pass
                            # print(type(ndvi_ravel2))
                            data = open(ndvi_ravel_dataset2,"r")
                            content = data.read()
                            ndvi_data2 = content.split(",")
                            for i in range(0, len(ndvi_data2),1):
                                if ndvi_data2[i] != '':
                                    ndvi_data2[i] = float(ndvi_data2[i])

                            #print(ndvi_data2)
                        else:
                            ndvi_data2 = '0'
                        #print(ndvi_ravel2)

                        json_data = {
                            "ndvi_name1":ndvi_name1,
                            "ndvi_ravel_dataset1":ndvi_data,
                            "max_ndvi_dataset1":max_ndvi_dataset1,
                            "min_ndvi_dataset1":min_ndvi_dataset1,
                            "ndvi_name2":ndvi_name2,
                            "ndvi_ravel_dataset2": ndvi_data2,
                            "max_ndvi_dataset2":max_ndvi_dataset2,
                            "min_ndvi_dataset2":min_ndvi_dataset2,
                            "output_location":output_location,
                            "output_location2":output_location2
                        }
                        return json.dumps(json_data, cls=NumpyEncoder)
                    else:
                        return render_template('analysis.html',msg="Dataset2 is not processed for NDVI")
                else:
                        return render_template('analysis.html',msg="Dataset1 is not processed for NDVI")
            except Exception as error:
                print("Error=",error)
                return render_template("homepage.html")

@app.route('/show_ndre_info', methods=['GET','POST'])
def show_ndre_info():
        if request.method == 'POST':
            try:
                data = request.get_json()
                dataset1 = data['dataset1']
                dataset2 = data['dataset2']
                mycursor = connect_obj.cursor(buffered=True)
                sql = "SELECT * FROM output WHERE Dataset_id = %s" 
                val = (dataset1,)
                no_of_rows = mycursor.execute(sql,val)
                myresult = mycursor.fetchone()
                if myresult != None:
                    ndre_name1 = myresult[2]
                    ndre_ravel_dataset1 = myresult[5]
                    max_ndre_dataset1 = myresult[10]
                    min_ndre_dataset1 = myresult[11]
                    output_location = myresult[16]
                    if ndre_ravel_dataset1 != '0':
                        data = open(ndre_ravel_dataset1,"r")
                        content = data.read()
                        ndre_data = content.split(",")
                        for i in range(0, len(ndre_data),1):
                            if ndre_data[i] != '':
                                ndre_data[i] = float(ndre_data[i])
                    else:
                        ndre_data = '0'
                    sql2 = "SELECT * FROM output WHERE Dataset_id = %s" 
                    val2 = (dataset2,)
                    no_of_rows2 = mycursor.execute(sql,val2)
                    myresult2 = mycursor.fetchone()
                    if myresult2 != None:
                        ndre_name2 = myresult2[2]
                        ndre_ravel_dataset2 = myresult2[5]
                        max_ndre_dataset2 = myresult2[10]
                        min_ndre_dataset2 = myresult2[11]
                        output_location2 = myresult2[16]
                        if ndre_ravel_dataset2 != '0':
                            data = open(ndre_ravel_dataset1,"r")
                            content = data.read()
                            ndre_data2 = content.split(",")
                            for i in range(0, len(ndre_data2),1):
                                if ndre_data2[i] != '':
                                    ndre_data2[i] = float(ndre_data2[i])
                        else:
                            ndre_data2 = '0'
                        print(output_location2)
                        json_data = {
                            'ndre_name1':ndre_name1,
                            'ndre_ravel_dataset1': ndre_data,
                            'max_ndre_dataset1':max_ndre_dataset1,
                            'min_ndre_dataset1':min_ndre_dataset1,
                            'ndre_name2':ndre_name2,
                            'ndre_ravel_dataset2': ndre_data2,
                            'max_ndre_dataset2':max_ndre_dataset2,
                            'min_ndre_dataset2':min_ndre_dataset2,
                            'output_location':output_location,
                            'output_location2':output_location2
                        }

                        return json.dumps(json_data, cls=NumpyEncoder)
                    else:
                        return render_template('analysis.html',msg="Dataset2 is not processed for NDVI")
                else:
                        return render_template('analysis.html',msg="Dataset1 is not processed for NDVI")
            except Exception as error:
                print("Error=",error)
                return render_template("homepage.html")

@app.route('/show_evi_info', methods=['GET','POST'])
def show_evi_info():
        if request.method == 'POST':
            try:
                data = request.get_json()
                dataset1 = data['dataset1']
                dataset2 = data['dataset2']
                mycursor = connect_obj.cursor(buffered=True)
                sql = "SELECT * FROM output WHERE Dataset_id = %s" 
                val = (dataset1,)
                no_of_rows = mycursor.execute(sql,val)
                myresult = mycursor.fetchone()
                if myresult != None:
                    evi_name1 = myresult[2]
                    evi_ravel_dataset1 = myresult[6]
                    max_evi_dataset1 = myresult[12]
                    min_evi_dataset1 = myresult[13]
                    output_location = myresult[16]
                    if evi_ravel_dataset1 != '0':
                        data = open(evi_ravel_dataset1,"r")
                        content = data.read()
                        evi_data = content.split(",")
                        for i in range(0, len(evi_data),1):
                            if evi_data[i] != '':
                                evi_data[i] = float(evi_data[i])
                    else:
                        evi_data = '0'
                    sql2 = "SELECT * FROM output WHERE Dataset_id = %s" 
                    val2 = (dataset2,)
                    no_of_rows2 = mycursor.execute(sql,val2)
                    myresult2 = mycursor.fetchone()
                    if myresult2 != None:
                        evi_name2 = myresult2[2]
                        evi_ravel_dataset2 = myresult2[6]
                        max_evi_dataset2 = myresult2[12]
                        min_evi_dataset2 = myresult2[13]
                        output_location2 = myresult2[16]
                        if evi_ravel_dataset2 != '0':
                            data = open(evi_ravel_dataset2,"r")
                            content = data.read()
                            evi_data2 = content.split(",")
                            for i in range(0, len(evi_data2),1):
                                if evi_data2[i] != '':
                                    evi_data2[i] = float(evi_data2[i])
                        else:
                            evi_data2 = '0'
                        json_data = {
                            'evi_name1':evi_name1,
                            'evi_ravel_dataset1': evi_data,
                            'max_evi_dataset1':max_evi_dataset1,
                            'min_evi_dataset1':min_evi_dataset1,
                            'evi_name2':evi_name2,
                            'evi_ravel_dataset2': evi_data2,
                            'max_evi_dataset2':max_evi_dataset2,
                            'min_evi_dataset2':min_evi_dataset2,
                            'output_location':output_location,
                            'output_location2':output_location2
                        }

                        return json.dumps(json_data, cls=NumpyEncoder)
                    else:
                        return render_template('analysis.html',msg="Dataset2 is not processed for evi")
                else:
                        return render_template('analysis.html',msg="Dataset1 is not processed for evi")
            except Exception as error:
                print("Error=",error)
                return render_template("homepage.html")

@app.route('/show_savi_info', methods=['GET','POST'])
def show_savi_info():
        if request.method == 'POST':
            try:
                data = request.get_json()
                dataset1 = data['dataset1']
                dataset2 = data['dataset2']
                mycursor = connect_obj.cursor(buffered=True)
                sql = "SELECT * FROM output WHERE Dataset_id = %s" 
                val = (dataset1,)
                no_of_rows = mycursor.execute(sql,val)
                myresult = mycursor.fetchone()
                if myresult != None:
                    savi_name1 = myresult[2]
                    savi_ravel_dataset1 = myresult[7]
                    max_savi_dataset1 = myresult[14]
                    min_savi_dataset1 = myresult[15]
                    output_location = myresult[16]
                    if savi_ravel_dataset1 != '0':
                        data = open(savi_ravel_dataset1,"r")
                        content = data.read()
                        savi_data = content.split(",")
                        for i in range(0, len(savi_data),1):
                            if savi_data[i] != '':
                                savi_data[i] = float(savi_data[i])
                    else:
                        savi_data = '0'
                    sql2 = "SELECT * FROM output WHERE Dataset_id = %s" 
                    val2 = (dataset2,)
                    no_of_rows2 = mycursor.execute(sql,val2)
                    myresult2 = mycursor.fetchone()
                    if myresult2 != None:
                        savi_name2 = myresult2[2]
                        savi_ravel_dataset2 = myresult2[7]
                        max_savi_dataset2 = myresult2[14]
                        min_savi_dataset2 = myresult2[15]
                        output_location2 = myresult2[16]
                        if savi_ravel_dataset2 != '0':
                            data = open(savi_ravel_dataset1,"r")
                            content = data.read()
                            savi_data2 = content.split(",")
                            for i in range(0, len(savi_data2),1):
                                if savi_data2[i] != '':
                                    savi_data2[i] = float(savi_data2[i])
                        else:
                            savi_data2 = '0'
                        json_data = {
                            'savi_name1':savi_name1,
                            'savi_ravel_dataset1': savi_data,
                            'max_savi_dataset1':max_savi_dataset1,
                            'min_savi_dataset1':min_savi_dataset1,
                            'savi_name2':savi_name2,
                            'savi_ravel_dataset2': savi_data2,
                            'max_savi_dataset2':max_savi_dataset2,
                            'min_savi_dataset2':min_savi_dataset2,
                            'output_location':output_location,
                            'output_location2':output_location2
                        }
                        return json.dumps(json_data, cls=NumpyEncoder)
                    else:
                        return render_template('analysis.html',msg="Dataset2 is not processed for savi")
                else:
                        return render_template('analysis.html',msg="Dataset1 is not processed for savi")
            except Exception as error:
                print("Error=",error)
                return render_template("homepage.html")


@app.route('/feedback_store',methods=['GET','POST'])
def feedback_home():
    if request.method == 'POST':
        email = request.form['user_email_contact']
        contact = request.form['user_contact']
        feedback = request.form['feedback']
        user_id = session['id']
        dated = datetime.datetime.now()
        mycursor = connect_obj.cursor()
        try:
            sql = "INSERT INTO feedback (User_id, Email_Id, Contact, Feedback, Dated) VALUES (%s, %s, %s, %s, %s)"
            val = (user_id, email, contact, feedback, dated)
            mycursor.execute(sql, val)
            connect_obj.commit()
            return render_template("homepage.html")
        except:
            return render_template("homepage.html",msg="Error storing the feeback")

@app.route('/feedback_store_new',methods=['GET','POST'])
def feedback_process():
    if request.method == 'POST':
        email = request.form['user_email_contact']
        contact = request.form['user_contact']
        feedback = request.form['feedback']
        user_id = session['id']
        dated = datetime.datetime.now()
        mycursor = connect_obj.cursor()
        try:
            sql = "INSERT INTO feedback (User_id, Email_Id, Contact, Feedback, Dated) VALUES (%s, %s, %s, %s, %s)"
            val = (user_id, email, contact, feedback, dated)
            mycursor.execute(sql, val)
            connect_obj.commit()
            return render_template("processing.html")
        except:
            return render_template("processing.html",msg="Error storing the feeback")


# PROFILE
@app.route("/profile",methods=['GET','POST'])
def redirect_profile():
    if request.method == "POST" or request.method == "GET":
        return render_template("profile.html")


@app.route("/delete_dataset", methods=['GET','POST'])
def delete_dataset():
    if request.method == "POST":
        try:
            data = request.get_data(cache=True, as_text=True, parse_form_data=False)
            dataset_id=int(data);
            user_id = session['id']
            mycursor = connect_obj.cursor()

            sql = "DELETE FROM dataset WHERE Dataset_id = %s AND Id= %s"
            val = (dataset_id, user_id)
            mycursor.execute(sql, val)
            connect_obj.commit()

            sql2 = "DELETE FROM output WHERE Dataset_id = %s AND User_id=%s"
            val2=(dataset_id,user_id)
            mycursor.execute(sql2,val2)
            connect_obj.commit();

            return render_template('profile.html')
        except:
            return render_template('profile.html')

@app.route("/get_update_dataset_info", methods=['GET','POST'])
def get_update_dataset_detail():
    if request.method == 'POST':
        try:
            data = request.get_data(cache=True, as_text=True, parse_form_data=False)
            dataset_id=int(data);
            user_id = session['id']

            print(dataset_id)
            print(user_id)
            mycursor = connect_obj.cursor(buffered=True)

            sql = "SELECT * FROM dataset WHERE Dataset_id = %s AND Id = %s"
            val=(dataset_id,user_id)
            mycursor.execute(sql,val)
            myresult = mycursor.fetchone()
            if myresult != []:
                location = myresult[2]
                name = myresult[5]
                date= myresult[4]
                json_data = {
                'location':location,
                'name':name,
                'date':date
                }
                return json.dumps(json_data, cls=DateTimeEncoder)
        except:
            return render_template("profile.html")
    else:
        return render_template("profile.html")

@app.route("/get_user_details",methods=['GET','POST'])
def get_user_details():
    if request.method=='POST':
        try:
            user_id = session['id']
            mycursor = connect_obj.cursor(buffered=True)
            sql = "SELECT * FROM user_details WHERE Id = %s"
            val=(user_id,)
            mycursor.execute(sql,val)
            myresult = mycursor.fetchone()
            if myresult != []:
                email=myresult[1]
                doj=myresult[3]

                json_data={
                'email':email,
                'doj':doj
                }

                return json.dumps(json_data,cls=DateTimeEncoder)
        except:
            return render_template("profile.html")
    else:
        return render_template("profile.html")

# MAP VISUALS BELOW

@app.route("/map",methods=['GET','POST'])
def map_visual():
    if request.method=='POST' or request.method=='GET':
        return render_template("map_visual.html")
    else:
        return render_template("homepage.html")
if __name__ == '__main__':
    app.run(port=5001,debug=True)